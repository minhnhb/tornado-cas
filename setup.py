from setuptools import setup, find_packages
from tornado_cas import _VERSION

setup(
    name="tornado_cas",
    version=_VERSION,
    install_requires=["tornado"],
    packages=find_packages()
)
