# CasMixin

This mixin helps you work with any CAS service. Just look at the example below.

## Installation

```
pip install -e https://minhnhb@bitbucket.org/minhnhb/tornado-cas.git#egg=tornado_cas
```

## Usage 
    from tornado_cas import CasMixin
    import tornado.web
    import tornado.gen

    class LoginHandler(tornado.web.RequestHandler, CasMixin):
    	_CAS_SERVER_URL = "https://id.mwork.vn"

        @tornado.gen.engine
    	@tornado.web.asynchronous
	    def get(self):
    	    user = yield tornado.gen.Task(self.authenticate_redirect)
            # work with the user
        	
    class LogoutHandler(tornado.web.RequestHandler, CasMixin):
    	_CAS_SERVER_URL = "https://id.mwork.vn"

	    def get(self):
    	    self.logout(callback="http://localhost/")

